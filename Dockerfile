FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY ["churras-tri-api.csproj", ""]
RUN dotnet restore "./churras-tri-api.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "churras-tri-api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "churras-tri-api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
CMD ASPNETCORE_URLS=http://*:$PORT dotnet churras-tri-api.dll