﻿using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using churras_tri_api.Services;
using System.Threading.Tasks;
using churras_tri_api.Models;
using MongoDB.Driver;
using System;

namespace churras_tri_api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BarbecueController : ControllerBase
    {
        private readonly BarbecueService _barbecueService;

        public BarbecueController(BarbecueService barbecueService)
        {
            _barbecueService = barbecueService;
        }

        /// <summary>
        /// Retorna um churrasco a partir de seu id
        /// </summary>
        /// <param name="barbecueId">Id do churrasco específico</param>
        /// <response code="200">Retorna um objeto de churrasco</response>
        [HttpGet]
        [Route("/barbecue")]
        public async Task<ActionResult<Barbecue>> GetBarbecueById([FromQuery] string barbecueId)
        {
            try
            {
                return Ok(await _barbecueService.GetBarbecueById(barbecueId));
            }
            catch (Exception e)
            {
                Console.Write(e);
                return StatusCode(500, new { message = "Ocorreu algum erro na solicitação !" });
            }
        }

        /// <summary>
        /// Retorna uma lista de churrascos a partir de um id de usuário
        /// </summary>
        /// <param name="userId">Id de um usuário específico</param>
        /// <response code="200">Retorna uma lista com os churrascos criados do usuário</response>
        [HttpGet]
        public async Task<ActionResult<List<Barbecue>>> GetBarbecueListByUser([FromQuery] string userId)
        {
            try
            {
                var result = await _barbecueService.GetBarbecueListByUser(userId);
                return Ok(result);
            }
            catch (Exception e)
            {
                Console.Write(e);
                return StatusCode(500, new { message = "Ocorreu algum erro na solicitação !" });
            }
        }

        /// <summary>
        /// Insere um churrasco
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST
        ///     {
        ///        "date": "2019-12-31", 
        ///        "description": "string",
        ///        "additionalRemarks": ["string"],
        ///        "creatorUser": "string",
        ///        "participants": [{
        ///         "id": "string",
        ///         "contribution": int,
        ///         "willDrink": bool,
        ///         "drinkContribution": int
        ///        }]	
        ///     }
        ///
        /// </remarks>
        /// <param name="obj">Objeto de representação do churrasco</param>
        /// <response code="200">Retorna o objeto criado</response>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Barbecue obj)
        {
            try
            {
                var result = await _barbecueService.InsertBarbecue(obj);
                
                if (result == null) return BadRequest(new { message = "A data precisa ser maior que hoje!" });

                return Ok(result);
            }
            catch (Exception e)
            {
                Console.Write(e);
                return StatusCode(500, new { message = "Ocorreu algum erro na solicitação !" });
            }
        }

        /// <summary>
        /// Atualiza um churrasco
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT
        ///     {
        ///        "id": "string"
        ///        "date": "dateTime", 
        ///        "description": "string",
        ///        "additionalRemarks": ["string"],
        ///        "creatorUser": "string",
        ///        "participants": [{
        ///         "id": "string",
        ///         "contribution": int,
        ///         "willDrink": bool,
        ///         "drinkContribution": int
        ///        }]	
        ///     }
        ///
        /// </remarks>
        /// <param name="obj">Objeto de representação do churrasco</param>
        /// <response code="200">Retorna um objeto com alguns dados do usuário</response>
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] Barbecue obj)
        {
            try
            {
                var result = await _barbecueService.UpdateBarbecue(obj);

                if (result == null) return BadRequest(new { message = "A data precisa ser maior que hoje!" });
                
                return Ok(result);
            }
            catch (Exception e)
            {
                Console.Write(e);
                return StatusCode(500, new { message = "Ocorreu algum erro na solicitação !" });
            }
        }

        /// <summary>
        /// Deleta um churrasco a partir de seu id
        /// </summary>
        /// <param name="id">Id de um churrasco específico</param>
        [HttpDelete]
        public async Task<ActionResult> Delete([FromQuery]string id)
        {
            try
            {
                return Ok(await _barbecueService.DeleteBarbecue(id));
            }
            catch (Exception e)
            {
                Console.Write(e);
                return StatusCode(500, new { message = "Ocorreu algum erro na solicitação !" });
            }
        }
    }
}
