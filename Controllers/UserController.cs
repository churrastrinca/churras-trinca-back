﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using churras_tri_api.Services;
using churras_tri_api.Models;
using System.Threading.Tasks;
using System;

namespace churras_tri_api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Retorna uma lista com todos os usuários registrados
        /// </summary>
        /// <response code="200">Retorna umas lista com os usuários registrados</response>
        [HttpGet]
        public async Task<IActionResult> GetAllUsers()
        {
            try
            {
                var result = await _userService.GetAllUsers();
                if (result == null)
                    return NotFound(new { message = "Nenhum usuário encontrado" });
                return Ok(result);
            }
            catch (Exception e)
            {
                Console.Write(e);
                return StatusCode(500, new { message = "Ocorreu algum erro na solicitação !" });
            }
        }

        /// <summary>
        /// Efetua o login de um usuário
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /authenticate
        ///     {
        ///        "email": "string",
        ///        "password": "string"
        ///     }
        ///
        /// </remarks>
        /// <param name="obj">Objeto de representação do login</param>
        /// <response code="200">Retorna um objeto com alguns dados do usuário</response>
        [AllowAnonymous]
        [HttpPost("/authenticate")]
        public async Task<IActionResult> Authenticate([FromBody]Login obj)
        {
            try
            {
                var user = await _userService.Authenticate(obj.Email, obj.Password);

                if (user == null)
                    return NotFound(new { message = "Usuário ou Senha está incorreta" });

                return Ok(user);
            }
            catch (Exception e)
            {
                Console.Write(e);
                return StatusCode(500, new { message = "Ocorreu algum erro na solicitação !" });
            }
        }

        /// <summary>
        /// Insere um usuário no sistema
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /register
        ///     { 
        ///        "name": "string",
        ///        "email": "string",
        ///        "password": "string"
        ///     }
        ///
        /// </remarks>
        /// <param name="obj">Objeto de representação do usuário</param>
        /// <response code="200">Retorna um objeto com alguns dados do usuário</response>
        [AllowAnonymous]
        [HttpPost]
        [Route("/register")]
        public async Task<IActionResult> Post([FromBody] User obj)
        {
            try
            {                
                return Ok(await _userService.InsertUser(obj));
            }
            catch (Exception e)
            {
                Console.Write(e);
                return StatusCode(500, new { message = "Ocorreu algum erro na solicitação !" });
            }
        }
    }
}
