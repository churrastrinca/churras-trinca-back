﻿using System.ComponentModel.DataAnnotations;

namespace churras_tri_api.Models
{
    public class Login
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
