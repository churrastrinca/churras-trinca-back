
namespace churras_tri_api.Models
{
    public class ResultUser
    {        
        public string Id { get; set; }        
        public string Name { get; set; }
        public string Token { get; set; }
    }
}
