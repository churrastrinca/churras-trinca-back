﻿using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using MongoDB.Bson;
using System;

namespace churras_tri_api.Models
{
    public class Barbecue
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("date")]
        public DateTime Date { get; set; }
        [BsonElement("description")]
        public string Description { get; set; }
        [BsonElement("additionalRemarks")]
        public List<string> AdditionalRemarks { get; set; }
        [BsonElement("creatorUser")]
        public string CreatorUser { get; set; }
        [BsonElement("participants")]
        public List<Participants> Participants { get; set; }
    }
}
