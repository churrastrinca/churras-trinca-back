﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace churras_tri_api.Models
{
    public class Participants
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Contribution { get; set; }
        public bool WillDrink { get; set; }
        public int DrinkContribution { get; set; }
    }
}
