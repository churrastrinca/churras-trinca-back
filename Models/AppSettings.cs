namespace churras_tri_api.Models
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }
}
