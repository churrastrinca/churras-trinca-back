﻿using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;
using churras_tri_api.Models;
using MongoDB.Driver;
using System;

namespace churras_tri_api.Services
{
    public class BarbecueService
    {
        private MongoClient client;
        private IMongoDatabase database;
        private IMongoCollection<Barbecue> _barbecueCollection;

        public BarbecueService(IOptions<AppSettings> appSettings)
        {
            client = new MongoClient(appSettings.Value.ConnectionString);
            database = client.GetDatabase(appSettings.Value.Database);
            _barbecueCollection = database.GetCollection<Barbecue>("Barbecue");
        }

        public async Task<Barbecue> GetBarbecueById(string barbecueId)
        {
            return await _barbecueCollection.Find(b => b.Id == barbecueId).FirstOrDefaultAsync();
        }

        public async Task<List<Barbecue>> GetBarbecueListByUser(string userId)
        {
            return await _barbecueCollection
                .Find(b => b.CreatorUser == userId && b.Date > DateTime.UtcNow)
                .SortBy(x => x.Date)
                .ToListAsync();
        }

        public async Task<Barbecue> InsertBarbecue(Barbecue obj)
        {
            if (obj.Date < DateTime.UtcNow) return null;

            await _barbecueCollection.InsertOneAsync(obj);
            return obj;
        }

        public async Task<ReplaceOneResult> UpdateBarbecue(Barbecue barb)
        {
            if (barb.Date < DateTime.UtcNow) return null;

            return await _barbecueCollection.ReplaceOneAsync(b => b.Id == barb.Id, barb);
        }

        public async Task<DeleteResult> DeleteBarbecue(string id)
        {
            return await _barbecueCollection.DeleteOneAsync(b => b.Id == id);
        }
    }
}
