﻿using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Security.Claims;
using churras_tri_api.Models;
using MongoDB.Driver;
using System.Text;
using System;

namespace churras_tri_api.Services
{
    public class UserService
    {
        private MongoClient client;
        private IMongoDatabase database;
        private IMongoCollection<User> _userCollection;

        public UserService(IOptions<AppSettings> appSettings)
        {
            client = new MongoClient(appSettings.Value.ConnectionString);
            database = client.GetDatabase(appSettings.Value.Database);
            _userCollection = database.GetCollection<User>("User");
        }

        public async Task<List<ResultUser>> GetAllUsers()
        {
            List<ResultUser> listResult = new List<ResultUser>();
            var result = await _userCollection.Find(_ => true).ToListAsync();

            result.ForEach(x =>
            {
                listResult.Add(
                    new ResultUser
                    {
                        Id = x.Id,
                        Name = x.Name
                    }
                );
            });

            return listResult;
        }

        public async Task<ResultUser> InsertUser(User obj)
        {
            await _userCollection.InsertOneAsync(obj);
            return new ResultUser { Id = obj.Id, Name = obj.Name };
        }

        public async Task<ResultUser> Authenticate(string email, string password)
        {
            var user = await _userCollection.Find(x => x.Email == email && x.Password == password).FirstOrDefaultAsync();

            if (user == null)
                return null;

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("ChurrasTrincaSecret");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            ResultUser resUser = new ResultUser
            {
                Id = user.Id,
                Name = user.Name,
                Token = tokenHandler.WriteToken(token)
            };

            return resUser;
        }
    }
}
